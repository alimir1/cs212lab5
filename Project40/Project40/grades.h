#include <iostream>
/**
@file grades.h
this header file has implementation of class student and class course
@author Ali Mahmood Mir
@version 1.00
*/


#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;
/**@class CSVRow
this class implements CSVRow
*/
class CSVRow
{
	vector<string> m_data;///characteristics of each student is stored
public:
	vector<float> percentages;
	string const& operator[](size_t index) const ///operator overloading [] operator now return a members of array
	{
		return m_data[index];
	}
	size_t size() const	///simlification of size function so it returns the no of parameters in a row
	{
		return m_data.size();
	}
	void readNextRow(std::istream& str)
	{
		string line;
		getline(str, line);///str is being stored in line till \n
		stringstream lineStream(line);///changing line's type to make it compatible with the getline func ahead
		string cell;
		m_data.clear();///destroying all elements stored in mdata
		while (std::getline(lineStream, cell, ','))///delimiter is ','
		{
			m_data.push_back(cell);
		}
	}
};
istream& operator >> (istream& str, CSVRow& data)///csv file parsing
{
	data.readNextRow(str);
	return str;
}
