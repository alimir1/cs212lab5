#include "grades.h"


int main(int argc, char *argv[])
{
	ifstream file(argv[1]);///file is accessed through commandprompt
	CSVRow row;
	if (!file.is_open())///only inititates program if the file is found
	{
		cout << "Could not Open .csv file... Program will now terminate" << endl << endl;
		cin.get();
		exit(0);
	}
	while (file >> row)///calculates and prints the grade of all the students enrolled in the course
	{
		int i = 2;
		float a;
		row.percentages.clear();

		while (i < 13)
		{

			a = stof(row[i]); ///converting string to float
			row.percentages.push_back(a);
			i += 2;
		}
		float percAss1 = row.percentages[1] * 0.05;///calculating final percentage
		float percAss2 = row.percentages[2] * 0.1;
		float percAss3 = row.percentages[3] * 0.15;
		float percMid = row.percentages[4] * 0.3;
		float percEse = row.percentages[5] * 0.4;
		float percTotal = percAss1 + percAss2 + percAss3 + percEse + percMid;

		if (percTotal >= 80)///test to check what grade the student got
		{
			cout << "Name:\t " << row[0] << " " << row[1] << endl << "ID:\t " << row[2] << endl << "Final:\t" << percTotal << "%" << endl << "Grade:\tH" << endl << endl;
		}
		if (percTotal >= 50 && percTotal < 80)
		{
			cout << "Name:\t" << row[0] << " " << row[1] << endl << "ID:\t " << row[2] << endl << "Final:\t" << percTotal << "%" << endl << "Grade:\tR" << endl << endl;

		}
		if (percTotal < 50)
		{
			cout << "Name:\t" << row[0] << " " << row[1] << endl << "ID:\t" << row[2] << endl << "Final:\t" << percTotal << "%" << endl << "Grade:\tF" << endl << endl;

		}
	}
	cout << endl;
	cin.get();
	return 0;
}
