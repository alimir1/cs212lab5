
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

class CSVRow
{
	vector<string> m_data;
public:
	vector<float> percentages;
	string const& operator[](size_t index) const
	{
		return m_data[index];
	}
	size_t size() const
	{
		return m_data.size();
	}
	void readNextRow(std::istream& str)
	{
		string line;
		getline(str, line);//str is being stored in line till \n
		stringstream lineStream(line);//changing line's type to make it compatible with the getline func ahead
		string cell;
		m_data.clear();//destroying all elements stored in mdata
		while (std::getline(lineStream, cell, ','))
		{
			m_data.push_back(cell);
		}
	}
};
istream& operator >> (istream& str, CSVRow& data)
{
	data.readNextRow(str);
	return str;
}