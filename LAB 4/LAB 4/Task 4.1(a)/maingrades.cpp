#include "grades.h"

int main(int argc, char *argv[])
{
	course CS212;
	ifstream infile(argv[1]);
	if (!infile.is_open())
	{
		cout << "Could not Open .csv file... Program will now terminate" <<endl<<endl;
		system("pause");
		exit(1);
	}
	system("pause");
	string line;
	vector<string> data;
	while (getline(infile, line)) //delimiter is '\n' by default
	{
		data.push_back(line);
	}
	for (int i = 0; i < 26; i++)
	{
		string str = data[i];
		istringstream customString(str); //converting string to istream to make it compatible with the getline func
		string field;
		student tempStudent;
		vector<string> singlestudentrecords;
		while (getline(customString, field, ','))
		{
			singlestudentrecords.push_back(field);
		}
		tempStudent.lastName = singlestudentrecords[0];
		tempStudent.firstName = singlestudentrecords[1];
		tempStudent.id = stoi(singlestudentrecords[2]);//converting string to int
		float a;
		a = stof(singlestudentrecords[4]); //converting string to float
		CS212.assignment.push_back(a);
		a = stof(singlestudentrecords[6]);
		CS212.assignment.push_back(a);
		a = stof(singlestudentrecords[8]);
		CS212.assignment.push_back(a);
		CS212.midTerm = stof(singlestudentrecords[10]);
		CS212.ese = stof(singlestudentrecords[12]);

		float percAss1 = CS212.assignment[0] * 0.05;
		float percAss2 = CS212.assignment[1] * 0.1;
		float percAss3 = CS212.assignment[2] * 0.15;
		float percMid = CS212.midTerm * 0.3;
		float percEse = CS212.ese *0.4;
		float percTotal = percAss1 + percAss2 + percAss3 + percEse + percMid;

		if (percTotal >= 80)
		{
			cout << "Name: " << tempStudent.lastName << " " << tempStudent.firstName << endl << "ID: " << tempStudent.id << endl << "Grade: H"<<endl<<endl;
		}
		if (percTotal >= 50 && percTotal < 80)
		{
			cout << "Name: " << tempStudent.lastName << " " << tempStudent.firstName << endl << "ID: " << tempStudent.id << endl << "Grade: R"<<endl<<endl;

		}
		if (percTotal<50)
		{
			cout << "Name: " << tempStudent.lastName << " " << tempStudent.firstName << endl << "ID: " << tempStudent.id << endl << "Grade: F"<<endl<<endl;

		}

	}
	cout << endl;

	system("pause");
	return 0;
}
